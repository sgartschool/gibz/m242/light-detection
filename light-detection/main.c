#include "atmega328p.h"
#include "AvrLib.h"
#include "RegisterAccess.h"
#include "math.h"

typedef enum {
	CmdSwitchLED = 0xA,
	CmdTestManchester = 0xB
} AvrCommands;

Bool HandleMessageFromPc(const AvrMessage* msg);

void SetPortConfigurationTransmit(void);
void SetPortConfigurationReceive(void);
void ConfigureAdc(void);

void SetLed(Bool on) {
	UpdateRegister(PortC.PORT, (PIN_1, on));
}

int main(void) {
    Usart_Init(250000);
	
	TRACE("Hello World");
	SetPortConfigurationReceive();
	ConfigureAdc();
	RegisterCompareMatchInterrupt(ExtInterruptSource0, TimerFrequency_Div1024, 125, 0);

	RegisterAvrMessageHandler(HandleMessageFromPc);

	InitializeStateEventFramework();
	return 0;
}

void sendData(int input) {
	int bitsToSend = EncodeManchester(input); // TODO: Receive Number, not array

	for (int i = 0; i < countBits(bitsToSend) > 0; i++) {
		SetLed((input >> i) & 1);
		// TODO: Wait, or setup timer
	}
}

void receiveData() {
	int receivedData = DecodeManchester(1); // TODO: Actually Receive Data

	// TODO: Do something with that data (send to server preferably)
}

int countBits(int n) {
	int count = 0;
	while (n) {
		count++;
		n >>= 1;
	}
	return count + 1;
}

int DecodeManchester(int input) {
	// sample input: 25958 (69, but encoded)
	int countOfBits = countBits(input);
	int temp[countOfBits / 2];

	// process bits
	for (int i = 0; i < countOfBits; i += 2) {
	    
	    int firstBit  = (input >> i) & 1;
	    int secondBit = (input >> (i + 1)) & 1;
	    
	         if (firstBit == 0 && secondBit == 1) temp[i / 2] = 1;
	    else if (firstBit == 1 && secondBit == 0) temp[i / 2] = 0;
	}
	
	// convert bits to interger
    int result = 0;
	for (int i = (countOfBits/2 - 1); i >= 0; i--)
        result += (temp[i] == 1) ? pow(2, i) : 0;

	return result;
}

int EncodeManchester(int input) {
	// sample input: 6 (0x0110)

	int countOfBits = countBits(input);
	int output[countOfBits * 2];

	for (int i = countOfBits * 2; i >= 0; i -= 2) {
		if ((input >> (i / 2)) & 1) {
            output[i]     = 1;
            output[i + 1] = 0;
        } else {
            output[i]     = 0;
            output[i + 1] = 1;
        }
        
        printf("%d", output[i]);
        printf("%d", output[i + 1]);
	}

	// TODO: Output number instead of array

	return output;
}

void SetPortConfigurationTransmit() {
	UpdateRegister(PortC.DDR, (PIN_0, 1), (PIN_1, 1));
	UpdateRegister(PortC.PORT, (PIN_0,0), (PIN_1, 0));
}

void SetPortConfigurationReceive() {
	UpdateRegister(PortC.DDR, (PIN_0, 1), (PIN_1, 0));
	UpdateRegister(PortC.PORT, (PIN_0,1), (PIN_1, 0));
}

Bool HandleMessageFromPc(const AvrMessage* msg )
{
	if (DefaultMessageHandler(msg))
		return True;

	if (msg->MsgType == PacketType_TestCommand) {
		if (msg->Payload[0] == CmdSwitchLED) {
			UnregisterCompareMatchInterrupt(CompareMatchSource1);
			SetPortConfigurationTransmit();
			SetLed(msg->Payload[1]!= 0);
			return True;
		}
		else if (msg->Payload[0] == CmdTestManchester)  {
			int bits[4] = {1, 0, 1, 1};
			for (int i = 0; i < 4 ; i++)
				SetLed(bits[i] != 0);
		}
	}
		
	return True;
}

void ConfigureAdc(void) {
	UpdateRegister(Adc.Admux, (ADMUX_MUX, MuxAdc1), (ADMUX_REFS, Internal1_1));
	UpdateRegister(Adc.Adcsra, (ADCSRA_ADATE,1), (ADCSRA_ADEN,1),  (ADCSRA_ADIE,1), (ADCSRA_ADPS, AdcDiv64));
	UpdateRegister(Adc.Adcsrb, (ADCSRB_ADTS, Tc0Compare));
}

ISR_AdcComplete() {
	uint16_t data = Adc.Data;
	TRACE( "@plot%16", data );
}